# title: "Tercer análisis"
# author: Julen San Tirso Hernández

rm(list = ls())
setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
getwd()

library(ade4)
library(FactoMineR)

#Leo los datos
entradas2=read.csv("entradas2.csv", header=TRUE, sep=",")
compras<-entradas2[c("Fecha.de.Operacion","Hora.operacion","Titulo",
                       "Precio.Base.Ticket","Dtos..Ticket","Forma.de.pago","Canal",
                       "C.P..Pagador","id_asistente", "genero")]


# Renombro todas las columnas, y así facilito el uso de las mismas
colnames(compras)[1]<-"fechaCompra"
colnames(compras)[2]<-"horaCompra"
colnames(compras)[3]<-"producto"
colnames(compras)[4]<-"precioBase"
colnames(compras)[5]<-"descuento"
colnames(compras)[6]<-"metodoPago"
colnames(compras)[7]<-"canalPago"
colnames(compras)[8]<-"CPasistente"
colnames(compras)[9]<-"idAsistente"
colnames(compras)[10]<-"genero" # <-- Ya se llamaba así

# Creo las fechas de los anuncios con sus anuncios
fechasNdP<-data.frame(Date=as.Date(character()), Characters=character())
colnames(fechasNdP)[1]<-"fecha_anuncio"
colnames(fechasNdP)[2]<-"desc_anuncio"
fechasNdP<-rbind(fechasNdP,data.frame(fecha_anuncio="10/11/2016",desc_anuncio="Depeche Mode"))
fechasNdP<-rbind(fechasNdP,data.frame(fecha_anuncio="11/14/2016",desc_anuncio="Coque Malla"))
fechasNdP<-rbind(fechasNdP,data.frame(fecha_anuncio="11/17/2016",desc_anuncio="Phoenix"))
fechasNdP<-rbind(fechasNdP,data.frame(fecha_anuncio="12/15/2016",desc_anuncio="The Killers"))
fechasNdP<-rbind(fechasNdP,data.frame(fecha_anuncio="12/26/2016",desc_anuncio="Pop up store"))
fechasNdP<-rbind(fechasNdP,data.frame(fecha_anuncio="01/03/2017",desc_anuncio="Justice"))
fechasNdP<-rbind(fechasNdP,data.frame(fecha_anuncio="01/26/2017",desc_anuncio="Brian Wilson"))
fechasNdP<-rbind(fechasNdP,data.frame(fecha_anuncio="02/02/2017",desc_anuncio="Basoa"))
fechasNdP<-rbind(fechasNdP,data.frame(fecha_anuncio="03/16/2017",desc_anuncio="Cartel por días"))
fechasNdP<-rbind(fechasNdP,data.frame(fecha_anuncio="03/17/2017",desc_anuncio="Iberian Festival Awards"))
fechasNdP<-rbind(fechasNdP,data.frame(fecha_anuncio="03/27/2017",desc_anuncio="Joe Goddard"))
fechasNdP<-rbind(fechasNdP,data.frame(fecha_anuncio="03/04/2017",desc_anuncio="Kele Okereke"))
fechasNdP<-rbind(fechasNdP,data.frame(fecha_anuncio="05/17/2017",desc_anuncio="Cierre cartel"))
fechasNdP<-rbind(fechasNdP,data.frame(fecha_anuncio="05/24/2017",desc_anuncio="Heineken"))
fechasNdP<-rbind(fechasNdP,data.frame(fecha_anuncio="05/30/2017",desc_anuncio="Convocatoria Hirian"))
fechasNdP<-rbind(fechasNdP,data.frame(fecha_anuncio="05/31/2017",desc_anuncio="Hirian"))
fechasNdP<-rbind(fechasNdP,data.frame(fecha_anuncio="06/01/2017",desc_anuncio="Cashless"))
fechasNdP<-rbind(fechasNdP,data.frame(fecha_anuncio="06/21/2017",desc_anuncio="Horarios"))
fechasNdP<-rbind(fechasNdP,data.frame(fecha_anuncio="06/22/2017",desc_anuncio="Bereziak"))
fechasNdP<-rbind(fechasNdP,data.frame(fecha_anuncio="07/07/2017",desc_anuncio="1ª jornada"))
fechasNdP<-rbind(fechasNdP,data.frame(fecha_anuncio="07/08/2017",desc_anuncio="2ª jornada"))
fechasNdP<-rbind(fechasNdP,data.frame(fecha_anuncio="07/09/2017",desc_anuncio="3ª jornada"))

# 1. Preparo la fecha_compra como Date.
compras$fechaCompra <- as.character(compras$fechaCompra)
compras$anyo = substring(compras$fechaCompra, 7, 10)
compras$mes  = substring(compras$fechaCompra, 4, 5)
compras$dia  = substring(compras$fechaCompra, 1, 2)
compras$fechaCompra = paste(compras$mes,compras$dia,compras$anyo,sep="/")

# 2. Preparo la "fecha_anuncio"
fechasNdP$fecha_anuncio = as.character(fechasNdP$fecha_anuncio)
fechasNdP$fecha_siguiente = as.Date(fechasNdP$fecha_anuncio, "%m/%d/%Y")+1
fechasNdP$fecha_siguiente = as.character(fechasNdP$fecha_siguiente)
fechasNdP$anyo = substring(fechasNdP$fecha_siguiente, 1, 4)
fechasNdP$mes  = substring(fechasNdP$fecha_siguiente, 6, 7)
fechasNdP$dia  = substring(fechasNdP$fecha_siguiente, 9, 10)
fechasNdP$fecha_siguiente = paste(fechasNdP$mes,fechasNdP$dia,fechasNdP$anyo,sep="/")
str(fechasNdP)
fechasNdP$anyo = NULL
fechasNdP$mes  = NULL
fechasNdP$dia  = NULL

# Mezclo ambos a ver qué puedo encontrar
compras3 = merge(compras,fechasNdP,by.x="fechaCompra",by.y="fecha_siguiente",all.x=TRUE,all.y=FALSE)
compras3$anuncio = ifelse(!is.na(compras3$desc_anuncio),1,0)

#Elimino las columnas que no me interesan despues de tomar la decisión de lo que quiero calcular
compras3$fecha_anuncio=NULL
compras3$fecha_siguiente=NULL
compras3$anyo=NULL
compras3$mes=NULL
compras3$dia=NULL
compras3$CPasistente=NULL
compras3$idAsistente=NULL
compras3$fechaCompra=NULL
compras3$horaCompra=NULL
compras3$precioBase=NULL

#Me quedo solo con los bonos de 3 días, ya que, mi interes es saber que puede afectar a la compra de dichos bonos
parte1= compras3[compras3$producto =="BILBAO BBK LIVE 17 Bono 3 Días",]
parte2= compras3[compras3$producto =="2 PAGO BILBAO BBK LIVE 17 Bono 3 Dias",]
Compras_def= rbind(parte1,parte2)
Compras_def$producto=NULL

compras3=Compras_def

# 5. Preparo el descuento
str(compras3$descuento)
compras3$descuento <- as.character(compras3$descuento)
compras3$descuento <- substr(compras3$descuento,1,regexpr(",",compras3$descuento)-1)
compras3$descuento <- as.numeric(compras3$descuento)
compras3$descuento <- abs(compras3$descuento)

########################################################################################################################
########################################################################################################################

# 6. Preparo el método de pago
compras3$metodoPago <- ifelse(compras3$metodoPago=="CAJERO",1,
                              ifelse(compras3$metodoPago=="CECA Seguro",2,
                                     ifelse(compras3$metodoPago=="Datafono",3,
                                            ifelse(compras3$metodoPago=="Devoluciones Telekutxa",4,
                                                   ifelse(compras3$metodoPago=="Efectivo",5,
                                                          ifelse(compras3$metodoPago=="Invitacion",6,
                                                                 ifelse(compras3$metodoPago=="Sermepa Seguro",7,
                                                                        ifelse(compras3$metodoPago=="TPV-Pago Final",8,
                                                                               ifelse(compras3$metodoPago=="TPV-Reserva",9,
                                                                                      ifelse(compras3$metodoPago=="TPV CECA Janto Extranj/PIN",10,0))))))))))

# 7. Preparo el canal de pago
compras3$canalPago <- ifelse(compras3$canalPago=="ATM",1,
                             ifelse(compras3$canalPago=="BKLIVP",2,
                                    ifelse(compras3$canalPago=="BKLVP",3,
                                           ifelse(compras3$canalPago=="TAQ",4,
                                                  ifelse(compras3$canalPago=="WEB",5,
                                                         ifelse(compras3$canalPago=="WKB",6,0))))))


# 8. Preparamos el anuncio
compras3$desc_anuncio <- ifelse(compras3$desc_anuncio=="Depeche Mode",1,
                              ifelse(compras3$desc_anuncio=="Justice",2,
                                     ifelse(compras3$desc_anuncio=="Joe Goddard",3,
                                            ifelse(compras3$desc_anuncio=="Hirian",4,
                                                   ifelse(compras3$desc_anuncio=="2ª jornada",5,
                                                          ifelse(compras3$desc_anuncio=="Coque Malla",6,
                                                                 ifelse(compras3$desc_anuncio=="Brian Wilson",7,
                                                                        ifelse(compras3$desc_anuncio=="Kele Okereke",8,
                                                                               ifelse(compras3$desc_anuncio=="Cashless",9,
                                                                                      ifelse(compras3$desc_anuncio=="3ª jornada",10,
                                                                                             ifelse(compras3$desc_anuncio=="Basoa",11,
                                                                                                    ifelse(compras3$desc_anuncio=="Phoenix",12,
                                                                                                           ifelse(compras3$desc_anuncio=="Cierre cartel",13,
                                                                                                                  ifelse(compras3$desc_anuncio=="Horarios",14,
                                                                                                                         ifelse(compras3$desc_anuncio=="The Killers",15,
                                                                                                                                ifelse(compras3$desc_anuncio=="Cartel por días",16,
                                                                                                                                       ifelse(compras3$desc_anuncio=="Heineken",17,
                                                                                                                                              ifelse(compras3$desc_anuncio=="Bereziak",18,
                                                                                                                                                     ifelse(compras3$desc_anuncio=="Pop up store",19,
                                                                                                                                                            ifelse(compras3$desc_anuncio=="Iberian Festival Awards",20,
                                                                                                                                                                   ifelse(compras3$desc_anuncio=="Convocatoria Hirian",21,
                                                                                                                                                                          ifelse(compras3$desc_anuncio=="1ª jornada",22,0))))))))))))))))))))))

# 9. Preparamos el género
compras3$genero <- ifelse(compras3$genero=="female",1,
                                ifelse(compras3$genero=="male",2,0))

# Discretizo las variables, las centro
compras3=scale(compras3)

# Ahora compruebo el analisis de componentes generales de la matriz de compras, de esa manera vere de forma más visual
# como se correlacionan las variables entre si
PCA(compras3)

# Creo una distancia con el metodo manhattan para poder hacer el Hclust con ello, y asi ver los posibles grupos que se forman
d = dist(compras3, method = "manhattan")
hc = hclust(d,method = "complete")
str(hc)

#miro como se han juntado los elementos, se juntan los elementos que ponen el negativo delante, y los positivos son las uniones hechas en esa fila
hc$merge

# dibujo el dendrograma
plot(hc)

# Despues de mirar y decidir cual sera el punto de corte que quiero (basandome en la clasificacion con los mejores grupos), pasamos a cortar el arbol
cutree(hc,5)

#Dibujo el gráfico
plot(compras3)
plot(compras3,col= cutree(hc,5))

#Viendo que los resultados no nos dan un gran aporte a nuestra condición, procedo a mirar las tablas poco a poco para obtener mejores resultados
# Coloco el descuento en valor absoluto en la matriz inicial, que posee los nombres de los grupos y metodos de pago

str(Compras_def$descuento)
Compras_def$descuento <- as.character(Compras_def$descuento)
Compras_def$descuento <- substr(Compras_def$descuento,1,regexpr(",",Compras_def$descuento)-1)
Compras_def$descuento <- as.numeric(Compras_def$descuento)
Compras_def$descuento <- abs(Compras_def$descuento)

plot(Compras_def)

desc=as.data.frame(table(Compras_def$descuento))
metodo=as.data.frame(table(Compras_def$metodoPago))
canal=as.data.frame(table(Compras_def$canalPago))
genero=as.data.frame(table(Compras_def$genero))
anuncio= as.data.frame(table(Compras_def$desc_anuncio))
anun=as.data.frame(table(Compras_def$anuncio))




