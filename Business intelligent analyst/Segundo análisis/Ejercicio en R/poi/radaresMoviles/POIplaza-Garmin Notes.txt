  ENGLISH

1 Unpack the file you have downloaded from POIplaza. 
2 Garmin POI Loader is required to import created customized points of interest. Details at:
  http://www.garmin.com/products/poiloader/.

Note: The POI may be displayed as an inaccessible location if the map you are using is not sufficiently detailed. 

  MAGYAR

1 Csomagolja ki a let�lt�tt �llom�nyt.
2 A let�lt�tt �llom�nyt a POI Loader seg�ts�g�vel konvert�lja saj�t navig�ci�s k�sz�l�k�n POI �llom�nny�. R�szletek:
  http://www.garmin.com/products/poiloader/.

Megjegyz�s: el�fordulhat, hogy a POI el�rhetetlen helyen fekszik, ha az �n �ltal haszn�lt t�rk�p nem kell�en r�szletes. 

www.poiplaza.com

All right reserved �